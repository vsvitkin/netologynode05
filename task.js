/************************
Vasily Svitkin 2017
Netology NodeJS course
Home work 5
*/
const querystring = require('querystring');
 function requestHandler(req,res){
   console.log('Processin request...');
   let data='';
   req.on('data',chunk=>data+=chunk);
   req.on('end',()=>{
     let d = querystring.parse(data);

      const firstName=d['firstName'];
      const lastName=d['lastName'];

      calAPI(firstName, lastName,(json)=>{
      answer = {
      "firstName": firstName,
      "lastName": lastName,
      "secretKey": JSON.parse(json)['hash']
      };

      res.writeHeader(200,'OK',{'Content-Type':'application/json'});

      res.write(JSON.stringify(answer));
      res.end();
  });
});

}

console.log('Tested with POST request to 127.0.0.1:5000 with data part firstName=Vasily&lastName=Svitkin');
console.log('Response was {');
console.log('  "firstName": "Vasily",');
console.log('  "lastName": "Svitkin",');
console.log('  "secretKey": "98b4bec9f1c44297ce1e2523ec94d04b"');
console.log('}');
const http = require('http');
const port=5000;
const server = http.createServer();
server.on('request',requestHandler);
server.on('listening',()=>{console.log('Server started on port '+port);});
server.listen(port);


function calAPI(firstName, lastName, callback){
console.log(`Processin request for ${firstName} ${lastName}...`);

data = JSON.stringify({'lastName':lastName});

let requestObject = {
  host:'netology.tomilomark.ru',
  port:80,
  method:'POST',
  path:'/api/v1/hash/',
  headers: {
  'Content-Type': 'Application/json',
  'Content-Length': Buffer.byteLength(data),
  'Firstname': firstName
  }
}

let request = http.request(requestObject);
request.write(data)
request.end();
 request.on('response',response => {
 let result=''
 response.on('data', chunk=>{
   result+=chunk;
 });
 response.on('end',()=>{callback(result)});
});
request.end();
}
